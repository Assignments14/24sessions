#### Setup and Run 

- Check setting in config/prod.php

- Build docker images for project
```bash
docker-compose build
```
- Run containers
```bash
docker-compose up -d
```
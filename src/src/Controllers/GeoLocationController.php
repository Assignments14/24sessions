<?php


namespace Geoloc\Controllers;


use Silex\Application;
use Symfony\Component\HttpFoundation\JsonResponse;
use Geoloc\Http\ErrorResponse;

final class GeoLocationController
{
    /** @var Application */
    private $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function getLocationForIp(): JsonResponse
    {
        $remoteIp = $this->app['service.clientip']->test();
        try {
            $ipinfo = json_decode($this->app['service.geoloc']->getForIp($remoteIp), true);
            $this->app['repository.geolocation']->saveIpInfo($ipinfo);
            return $this->app->json($ipinfo);
        } catch (\Exception $e) {
            return ErrorResponse::create($e);
        }
    }

    public function getClientIp(): JsonResponse
    {
        return $this->app->json($this->app['service.clientip']->real());
    }

    public function version(): JsonResponse
    {
        return $this->app->json($this->app['version']);
    }
}
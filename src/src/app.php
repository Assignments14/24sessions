<?php

use Silex\Application;
use Silex\Provider\AssetServiceProvider;
use Silex\Provider\ServiceControllerServiceProvider;
use Geoloc\Services\Location\{
    IpInfoService,
    ClientIpService
};
use Geoloc\Controllers\GeoLocationController;
use Geoloc\Repository\GeoLocationRepository;

$app = new Application();

$app->register(new ServiceControllerServiceProvider());
$app->register(new AssetServiceProvider());

/*
 * Services
 */
$app['service.geoloc'] = function() use ($app) {
    return new IpInfoService($app['ipinfo.url'], $app['ipinfo.token']);
};

$app['service.clientip'] = function () {
    return new ClientIpService();
};

/*
 * Repositories
 */
$app['repository.geolocation'] = function () {
    return new GeoLocationRepository();
};

/*
 * Controllers
 */
$app['controller.geoloc'] = function () use ($app) {
    return new GeoLocationController($app);
};

return $app;


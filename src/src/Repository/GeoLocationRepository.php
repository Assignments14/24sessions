<?php


namespace Geoloc\Repository;


use Geoloc\Model\GeoLocation;

class GeoLocationRepository
{
    public function saveIpInfo(array $ipinfo): void
    {

        $location = GeoLocation::find_by_ip($ipinfo['ip']);
        if (! $location) {
            $location = new GeoLocation();
        }

        $location->ip = $ipinfo['ip'];
        $location->country = $ipinfo['country'] ?? 'bogon';
        $location->city = $ipinfo['city'] ?? 'bogon';

        $location->save();
    }
}
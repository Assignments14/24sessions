<?php


namespace Geoloc\Services\Location;


class ClientIpService
{
    public function real(): string
    {
        $ip = $_SERVER['HTTP_CLIENT_IP']
            ?? $_SERVER['HTTP_X_FORWARDED_FOR']
            ?? $_SERVER['HTTP_X_FORWARDED']
            ?? $_SERVER['HTTP_FORWARDED_FOR']
            ?? $_SERVER['HTTP_FORWARDED']
            ?? $_SERVER['REMOTE_ADDR'];

        return $ip;
    }

    public function test(): string
    {
        return '178.133.47.119';
    }
}
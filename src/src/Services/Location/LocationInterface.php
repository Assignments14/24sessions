<?php


namespace Geoloc\Services\Location;


interface LocationInterface
{
    public function getForIp(string $ip): string;
}
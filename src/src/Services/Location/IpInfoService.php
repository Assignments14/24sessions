<?php


namespace Geoloc\Services\Location;


final class IpInfoService implements LocationInterface
{

    /** @var string */
    private $url;

    /** @var string */
    private $token;

    public function __construct(string $url, string $token)
    {
        $this->url = $url;
        $this->token = $token;
    }

    public function getForIp(string $ip): string
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $this->getFullUrl($ip));
        curl_setopt($ch, CURLOPT_HTTPHEADER, ['Accept: application/json']);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }

    public function getFullUrl(string $ip): string
    {
        return "{$this->url}/{$ip}?token={$this->token}";
    }
}
<?php


namespace Geoloc\Model;


use ActiveRecord\Model;

class GeoLocation extends Model
{
    public static $table_name = 'geolocation';

    public static $attr_protected = [
        'created_at', 'updated_at',
    ];


}
<?php


namespace Geoloc\Http;


use Symfony\Component\HttpFoundation\JsonResponse;

class ErrorResponse
{
    public static function create(\Exception $e): JsonResponse
    {
        return new JsonResponse([
            'code' => $e->getCode(),
            'message' => $e->getMessage(),
        ]);
    }
}
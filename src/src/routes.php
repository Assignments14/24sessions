<?php

use Symfony\Component\HttpFoundation\Request;

$app->get('/api/version', 'controller.geoloc:version');

$app->get('/api/getip', 'controller.geoloc:getClientIp');

$app->get('/api/getloc', 'controller.geoloc:getLocationForIp');

$app->error(function (\Exception $e, Request $request, $code) use ($app) {
    return $app->json([
        'code' => $code,
        'message' => $e->getMessage(),
    ]);
});


<?php

$app['version'] = '0.14';

$app['ipinfo.url'] = 'ipinfo.io';
$app['ipinfo.token'] = '95b6e4e0f2fd85';

$app['db.host'] = '172.19.0.1';
$app['db.database'] = '24sessions';
$app['db.port'] = '13306';
$app['db.user'] = '24sessions';
$app['db.password'] = '24sessions';

/*
 * Database init
 */
ActiveRecord\Config::initialize(function($cfg) use ($app)
{
    $modelDir = __DIR__ . '/../src/Model/';
    $cfg->set_model_directory($modelDir);
    $cfg->set_connections([
        'development' => "mysql://{$app['db.user']}:{$app['db.password']}@{$app['db.host']}:{$app['db.port']}/{$app['db.database']}",
    ]);
    $cfg->set_default_connection('development');
});
